import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Home from '../../containers/Home';

import data from './../../data/data.json'
import Person from './../../components/Person/Person.js';
import AdditionalInfo from './../../components/AdditionalInfo/AdditionalInfo.js';
import {BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom';
import Purchase from '../../components/Purchase/Purchase'




function modifyDatas(personData) {
    const positions = personData.positions
    const positionArray = positions.map(position => {
        const positionString = position.split(', ')
        const newPosition = [parseFloat(positionString[0]), parseFloat(positionString[1])]
        return newPosition
    });
    return positionArray;
}

const myDatas = data.map(personData => {
    const newPositions = modifyDatas(personData);
    const newPersonData = personData
    newPersonData.positions = newPositions
    return newPersonData;
});

const App = ( ) => { 

    return(
        <main>
            <Router>
                <div>
                    <nav>
                        <ul>
                        <li><Link to="/">Home</Link></li>
                        <li><Link to="/Person">Person</Link></li>
                        <li><Link to="/AdditionalInfo">AdditionalInfo</Link></li>
                        </ul>
                    </nav>
                    <Switch>
                        <Route path="Person"><Person /></Route>
                        <Route path="/AdditionalInfo"><AdditionalInfo /></Route>
                        <Route path="/"><Home myDatas={myDatas} /></Route>
                    </Switch>
                </div>
            </Router>
            
        </main>
    );

};

export default App;