import React from 'react';
import './SearchBar.css'; 

export default function SearchBar({names, handleChange}) {

    return <React.Fragment>
    <div>
        <input className="searchInput" list="names" id="names" onChange={handleChange}></input>
        <datalist id="names">
            {names.map((name, index) => <option value={name} key={index}></option>)}
        </datalist>
    </div>
    </React.Fragment>
}