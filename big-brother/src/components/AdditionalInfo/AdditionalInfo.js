import React from 'react';
import Modal from 'react-bootstrap/Modal';
import PersonMap from './../PersonMap/PersonMap.js'

export default function AdditionalInfo({personData, show, handleClick}) {

    return(
    
        <Modal show={show} onHide={handleClick}>
            <Modal.Header closeButton>
                <Modal.Title>{personData.name.first} {personData.name.last}</Modal.Title>
                <img src={personData.picture} alt="photo" />
            </Modal.Header>
            <Modal.Body>
                <p>{personData.address.street}</p>
                <p>{personData.address.city}</p>
                <p>{personData.address.state}</p>
            </Modal.Body>
            <Modal.Footer>
                <PersonMap positions={personData.positions}/>
            </Modal.Footer>
        </Modal>
    )
}
