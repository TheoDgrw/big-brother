import React, {useState} from 'react';


const Purchase = ({personData}) =>{

    const [displayPurchase, setDisplayPurchase] = useState(false)


    const handleClick = () => {
        setDisplayPurchase(!displayPurchase)
    }



    const purchaseFilter = displayPurchase ? personData.purchases.filter((purchase) => {
        return parseFloat(purchase.amount) > 3000       
    }) : personData.purchases; 


    return( 

          <div>
          <button onClick ={handleClick}>Purchase</button>
            {purchaseFilter.map((purchase) =>{
                return (<small> {purchase.amount} </small>)
            })}   
         </div>
    )
};

export default Purchase;